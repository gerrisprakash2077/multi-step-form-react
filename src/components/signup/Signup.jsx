import React, { Component } from 'react'
import './Signup.css'
export default class Signup extends Component {
    constructor(props) {
        super(props)
    }
    validate = () => {
        //first name
        let flag = true
        if (this.props.data.input.firstName.length >= 20) {
            this.props.updateError('firstName', "*First Name should not be more than 20 characters")
            flag = false
        }
        else if (this.props.data.input.firstName == "" || this.props.data.input.firstName == undefined || this.props.data.input.firstName == null) {
            this.props.updateError('firstName', "*First Name should not be empty")
            flag = false
        }
        else if (this.props.data.input.firstName.length < 3) {
            this.props.updateError('firstName', "*First Name should have atleast 3 characters")
            flag = false
        }
        else {
            this.props.updateError('firstName', '')
        }
        // last name
        if (this.props.data.input.lastName.length >= 20) {
            this.props.updateError('lastName', "*Last Name should not be more than 20 characters")
            flag = false
        }
        else if (this.props.data.input.lastName == "" || this.props.data.input.lastName == undefined || this.props.data.input.lastName == null) {
            this.props.updateError('lastName', "*Last Name should not be empty")
            flag = false
        }
        else if (this.props.data.input.lastName.length < 3) {
            this.props.updateError('lastName', "*last Name should have atleast 3 characters")
            flag = false
        }
        else {
            this.props.updateError('lastName', '')
        }
        //email
        if (this.props.data.input.email.length >= 40) {
            this.props.updateError('email', "*Last Name should not be more than 20 characters")
            flag = false
        }
        else if (this.props.data.input.email == "" || this.props.data.input.email == undefined || this.props.data.input.email == null) {
            this.props.updateError('email', "*email should not be empty")
            flag = false
        }
        else if (this.props.data.input.email.length < 3) {
            this.props.updateError('email', "*email should have atleast 3 characters")
            flag = false
        }
        else if (!this.props.data.input.email.includes('@')) {
            this.props.updateError('email', "*INVALID EMAIL")
            flag = false
        } else {
            this.props.updateError('email', '')
        }
        // dob
        if (!this.props.data.input.dob) {
            this.props.updateError('dob', "*DOB should not be empty")
            flag = false
        } else {
            this.props.updateError('dob', "")
        }
        //address
        if (this.props.data.input.address.length >= 180) {
            this.props.updateError('address', "*Address should not be more than 20 characters")
            flag = false
        }
        else if (this.props.data.input.address == "" || this.props.data.input.address == undefined || this.props.data.input.address == null) {
            this.props.updateError('address', "*Address should not be empty")
            flag = false
        }
        else if (this.props.data.input.address.length < 3) {
            this.props.updateError('address', "*Address should have atleast 3 characters")
            flag = false
        } else {
            this.props.updateError('address', "")
        }

        return flag
    }
    clickHandler = () => {
        if (this.validate()) {
            this.props.status()
            return this.props.handler('increase')
        }
    }
    render() {
        return (
            <div className='signup'>
                <div className='signup-container'>
                    <img src='https://c4.wallpaperflare.com/wallpaper/24/23/796/portrait-display-vertical-pattern-digital-art-wallpaper-preview.jpg' />
                    <div className='content'>
                        <div className="top-nav">
                            <p><span>{this.props.data.signup}</span>Sign UP</p>
                            <p><span id='signupMessage' >{this.props.data.message}</span>Message</p>
                            <p><span id='signupCheckbox'>{this.props.data.checkbox}</span>Checkbox</p>
                        </div>
                        <p>Step1/3</p>
                        <h2>Sign UP</h2>
                        <div className="input-container">
                            <div>
                                <label>First Name</label><br />
                                <input className={this.props.data.error.firstName ? 'red input' : ''} type='text' id="firstName" placeholder='Enter your First Name' onChange={this.props.updateData} value={this.props.data.input.firstName}></input>
                                <p id='error'>{this.props.data.error.firstName}</p>
                            </div>
                            <div>
                                <label>Last Name</label><br />
                                <input className={this.props.data.error.lastName ? 'red input' : ''} type='text' id="lastName" placeholder='Enter your Last Name' onChange={this.props.updateData} value={this.props.data.input.lastName}></input>
                                <p id='error'>{this.props.data.error.lastName}</p>
                            </div>
                            <div>
                                <label>Date of Birth</label><br />
                                <input className={this.props.data.error.dob ? 'red input' : ''} type='Date' id="dob" placeholder='Enter your Last Name' onChange={this.props.updateData} value={this.props.data.input.dob}></input>
                                <p id='error'>{this.props.data.error.dob}</p>
                            </div>
                            <div>
                                <label>Email Address</label><br />
                                <input className={this.props.data.error.email ? 'red input' : ''} type='email' id="email" placeholder='Enter your Email Address' onChange={this.props.updateData} value={this.props.data.input.email}></input>
                                <p id='error'>{this.props.data.error.email}</p>
                            </div>
                            <div>
                                <label>Address</label><br />
                                <input className={this.props.data.error.address ? 'red input' : ''} id="address" type='text' placeholder='Enter your Address' onChange={this.props.updateData} value={this.props.data.input.address}></input>
                                <p id='error'>{this.props.data.error.address}</p>
                            </div>
                        </div>
                        <button onClick={this.clickHandler}>Next Step</button>
                    </div>
                </div>
            </div>
        )
    }
}
