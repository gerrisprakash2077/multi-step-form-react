import React, { Component } from 'react'
import './Message.css'
export default class Message extends Component {
    validate = () => {
        let flag = true
        // let target = document.getElementById('messagebox')
        if (this.props.data.input.messagebox.length <= 3 || this.props.data.input.messagebox.length >= 280) {
            this.props.updateError('messagebox', 'Character count should be between 3 and 280')
            flag = false
        }
        else {
            this.props.updateError('messagebox', "")
        }
        //radio
        if (this.props.data.input.radio1 == false && this.props.data.input.radio2 == false) {
            this.props.updateError('radio1', 'Select any one option')
            flag = false
        }
        return flag
    }
    clickHandler = () => {
        if (this.validate()) {
            this.props.status()
            return this.props.handler('increase')
        }
    }
    render() {
        return (
            <div className='message'>
                <div className='message-container'>
                    <img src='https://c4.wallpaperflare.com/wallpaper/24/23/796/portrait-display-vertical-pattern-digital-art-wallpaper-preview.jpg' />
                    <div className='content'>
                        <div className="top-nav">
                            <p><span id="messageSignup">{this.props.data.signup}</span>Sign UP</p>
                            <p><span >{this.props.data.message}</span>Message</p>
                            <p><span id="messageCheckbox">{this.props.data.checkbox}</span>Checkbox</p>
                        </div>
                        <p>Step2/3</p>
                        <h2>Message</h2>
                        <div className="input-container">
                            <div>
                                <label>Message</label><br />
                                <textarea className={this.props.data.error.messagebox ? 'red input' : ''} id="messagebox" onChange={this.props.updateData} value={this.props.data.input.messagebox} rows="6" cols="50" />
                                <p id="error">{this.props.data.error.messagebox}</p>
                            </div>
                            <div className='messages-bottom-radio' >
                                <input type='radio' checked={this.props.data.input.radio1} onChange={this.props.updateData} value={this.props.data.radio1} id="radio1" name='choice'></input><label htmlFor='radio1' id="radio1Label">The number one choice</label>
                                <input type='radio' checked={this.props.data.input.radio2} onChange={this.props.updateData} value={this.props.data.radio2} id='radio2' name='choice'></input><label htmlFor='radio2'>The number two choice</label>
                                <p id="error">{this.props.data.error.radio1}</p>
                            </div>
                        </div>

                        <button onClick={this.clickHandler}>Next step</button>
                        <button id='messageBack' onClick={() => {
                            return this.props.handler('decrease')
                        }}>back</button>
                    </div>
                </div>
            </div>
        )
    }
}

