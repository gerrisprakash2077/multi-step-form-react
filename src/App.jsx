import React, { Component } from 'react'
import Checkbox from './components/checkbox/Checkbox'
import Message from './components/message/Message'
import Signup from './components/signup/Signup'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      page: 0,
      signup: 1,
      message: 2,
      checkbox: 3,
      input: {
        firstName: "",
        lastName: "",
        dob: "",
        email: "",
        address: "",
        messagebox: "",
        radio1: "",
        radio2: "",
        male: false,
        female: false,
        option1: false,
        option2: false
      },
      error: {
        firstName: "",
        lastName: "",
        dob: "",
        email: "",
        address: "",
        messagebox: "",
        radio1: "",
        radio2: "",
        male: '',
        female: ''
      }


    }
  }
  updateData = (event) => {
    let id = event.target.id
    switch (id) {
      case 'radio1':
        this.setState((previousState) => {
          previousState.input[id] = event.target.checked
          previousState.input['radio2'] = false
          return previousState
        })
        break
      case 'radio2':
        this.setState((previousState) => {
          previousState.input[id] = event.target.checked
          previousState.input['radio1'] = false
          return previousState
        })
        break
      case 'male':
        this.setState((previousState) => {
          previousState.input[id] = true
          previousState.input['female'] = false
          return previousState
        })
        break
      case 'female':
        this.setState((previousState) => {
          previousState.input[id] = true
          previousState.input['male'] = false
          return previousState
        })
        break
      case 'option1':
        this.setState((previousState) => {
          previousState.input[id] = true
          previousState.input['option2'] = false
          return previousState
        })
        break
      case 'option2':
        this.setState((previousState) => {
          previousState.input[id] = true
          previousState.input['option1'] = false
          return previousState
        })
        break
      default:
        this.setState((previousState) => {
          previousState.input[id] = event.target.value
          return previousState
        })
    }
  }
  updateError = (id, message) => {
    this.setState((previousState) => {
      previousState.error[id] = message
      return previousState
    })
  }
  changeSignup = () => {
    this.setState((previousState) => {
      previousState.signup = '✓'
      return previousState
    })
  }
  changeMessage = () => {
    this.setState((previousState) => {
      previousState.message = '✓'
      return previousState
    })
  }
  changeCheckbox = () => {
    this.setState((previousState) => {
      previousState.checkbox = '✓'
      return previousState
    })
  }
  changeState = (operation) => {
    if (operation == 'increase') {
      this.setState({
        page: this.state.page + 1
      })
    } else if (operation == 'decrease') {
      this.setState({
        page: this.state.page - 1
      })
    }
  }

  selectPage = () => {
    if (this.state.page == 0) {
      return <Signup updateData={this.updateData} updateError={this.updateError} status={this.changeSignup} data={this.state} handler={this.changeState} />
    } else if (this.state.page == 1) {
      return <Message updateData={this.updateData} updateError={this.updateError} status={this.changeMessage} data={this.state} handler={this.changeState} />
    } else if (this.state.page == 2) {
      return <Checkbox updateData={this.updateData} updateError={this.updateError} status={this.changeCheckbox} data={this.state} handler={this.changeState} />
    }
  }
  render() {
    return (
      <div>
        {this.selectPage()}
      </div>
    )
  }
}
